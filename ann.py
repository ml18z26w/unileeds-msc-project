import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from util.model_metric import modol_metrics
import matplotlib.pyplot as plt
import numpy as np


class ANNModel(nn.Module):

    def __init__(self, feature_dim, hidden_dim):
        super(ANNModel, self).__init__()
        self.fc1 = nn.Linear(feature_dim, hidden_dim)
        self.fc2 = nn.Linear(hidden_dim, hidden_dim)
        self.fc3 = nn.Linear(hidden_dim, 2)

    def forward(self, x):
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = self.fc3(x)
        return x

def ann_pred(net, testloader):
    net.eval()
    # evaluate model on testset
    pred_value = []
    true_value = []
    softmax = nn.Softmax()
    with torch.no_grad():
        for data in testloader:
            inputs, labels = data
            outputs = net(inputs.float())
            predict = softmax(outputs)
            # _, predict = torch.max(outputs.data, 1)
            pred_value += list(predict.numpy()[:, 1])
            true_value += list(labels.numpy().reshape(1, -1)[0])

    evals = modol_metrics(pred_value, true_value)
    return pred_value, evals

def ann_process(net, trainloader, testloader, load_model=True, if_save=False):
    torch.multiprocessing.freeze_support()

    # params
    learning_rate = 1e-5 # 0.001
    adam_weight_decay = 2e-5 # 0.008
    epoch_num = 10
    model_dir = '/Users/yuxin.lyx/Documents/practice/ing20200805_python_grad/pycodes/saved_models/ann'

    # net
    net.train()
    # define optimizer
    weight = torch.from_numpy(np.array([0.4, 0.6])).float()
    criterion = torch.nn.CrossEntropyLoss(weight=weight)
    # criterion = torch.nn.CrossEntropyLoss()
    optimizer = optim.AdamW(net.parameters(), lr=learning_rate, weight_decay=adam_weight_decay)
    scheduler = torch.optim.lr_scheduler.OneCycleLR(optimizer, max_lr=8e-6, steps_per_epoch=len(trainloader),  # 0.0008
                                                    epochs=epoch_num)
    # train
    if not load_model:
        print('Training Start')
        for epoch in range(epoch_num):
            running_loss = 0
            for i, data in enumerate(trainloader):
                inputs, labels = data
                optimizer.zero_grad()
                # forward
                outputs = net(inputs.float())
                # backward
                loss = criterion(outputs, labels.long())
                # optimize
                loss.backward()
                optimizer.step()
                scheduler.step()
                # statistics
                running_loss += loss.item()
                if i % 500 == 499:
                    _, test_evals = ann_pred(net, testloader)
                    print('[%d, %5d] train loss: %.10f, test_evals: %s' % (epoch + 1, i + 1, running_loss / 500, test_evals))
                    running_loss = 0.0

            state = {'model': net.state_dict(), 'optimizer': optimizer.state_dict(), 'scheduler': scheduler.state_dict(), 'epoch': epoch}
            if if_save:
                torch.save(state, "{}/equalweight{}.pth".format(model_dir, epoch))

        print('Training Finished')
    else:
        epoch = epoch_num - 1
        checkpoint = torch.load("{}/{}.pth".format(model_dir, epoch))
        net.load_state_dict(checkpoint.get('model'))

    pred_values, test_evals = ann_pred(net, testloader)
    return net, test_evals, pred_values
