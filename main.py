import pandas as pd
import numpy as np
import torch
from torch import optim
from sklearn.model_selection import train_test_split
from models.ann import ann_process, ann_pred, ANNModel
from models.mr import mr_process
from models.xgb import xgb_process
from models.lstm import lstm_process, LSTMModel, lstm_pred
from data.dataset import WeatherData, WeatherTimeSeriesData
from util.model_metric import modol_metrics
import torch.nn as nn
import copy

if __name__ == '__main__':
    torch.manual_seed(1)
    # params
    data_file_name = './inter_results/oulier_cleaned_data.csv'
    original_file_name = './data/weatherAUS.csv'
    label_name = 'RainTomorrow'
    batch_size = 32

    # read csv
    aus_data = pd.read_csv(data_file_name)

    # drop unwanted col
    cleaned_data = aus_data.drop(['RISK_MM'], axis=1)

    # split train and test dataset
    X = cleaned_data.drop([label_name], axis=1)
    Y = cleaned_data[label_name]
    print('The number of features: %s' % X.shape[1])

    # Split training and test sets chronologically
    train_x, test_x, train_y, test_y = train_test_split(X, Y, test_size=0.2, shuffle=False, random_state=0)

    # evaluate on test data
    # logistic regression
    lr_model, lr_evals = mr_process(train_x, train_y, test_x, test_y)
    print('The performance of logistic regression: %s' % lr_evals)

    # xgboost
    xgb_model, xgb_evals = xgb_process(train_x, train_y, test_x, test_y, load_stage1=True, load_stage2=True, load_final=True, plot_feat_imp=False)
    print('The performance of xgboost: %s' % xgb_evals)

    # ann
    hidden_dim = 32
    # trainset = WeatherData(train_x, train_y)
    # testset = WeatherData(test_x, test_y)
    # trainloader = torch.utils.data.DataLoader(trainset, batch_size=batch_size, shuffle=True, num_workers=2, drop_last=True)
    # testloader = torch.utils.data.DataLoader(testset, batch_size=batch_size, shuffle=True, num_workers=2, drop_last=True)
    # ann_model = ANNModel(train_x.shape[1], hidden_dim)
    # ann_model, ann_evals, ann_pred_values = ann_process(ann_model, trainloader, testloader, load_model=True)
    # print('The performance of ann: %s' % ann_evals)

    # lstm
    window_dim = 10
    hidden_dim = 32
    batch_size = 32
    net = LSTMModel(hidden_dim, batch_size, window_dim, train_x.shape[1])
    trainset = WeatherTimeSeriesData(train_x, train_y, window_dim)
    testset = WeatherTimeSeriesData(test_x, test_y, window_dim)
    trainloader = torch.utils.data.DataLoader(trainset, batch_size=batch_size, shuffle=True, num_workers=2, drop_last=True)
    testloader = torch.utils.data.DataLoader(testset, batch_size=batch_size, shuffle=False, num_workers=2, drop_last=True)
    lstm_model, lstm_evals, lstm_pred_values = lstm_process(net, trainloader, testloader, load_model=True)
    print('The performance of lstm: %s' % lstm_evals)

    # compute accuracy in different locations
    original_data = pd.read_csv(original_file_name)
    aus_data['Location'] = original_data['Location']
    _, test_data = train_test_split(aus_data, test_size=0.2, shuffle=False, random_state=0)
    test_data = test_data.iloc[0: len(test_data) - len(test_data) % batch_size, :]
    test_data['pred'] = lstm_pred_values
    locations = test_data['Location'].unique()
    # location_accuracy = pd.DataFrame(columns=['Location', 'f1score', 'precision', 'recall', 'accuracy', 'auc'])
    # n = 0
    # for loc in locations:
    #     _td = test_data[test_data['Location'] == loc]
    #     _metrics = modol_metrics(_td['pred'], _td[label_name])
    #     location_accuracy.loc[n] = [loc, _metrics.get('f1score'), _metrics.get('precision'), _metrics.get('recall'),
    #                                 _metrics.get('accuracy'), _metrics.get('auc')]
    #     n += 1
    # print(location_accuracy)
    # location_accuracy.to_csv('./inter_results/location_results_before_transferlearning.csv')

    # transfer learning
    cleaned_data['Location'] = aus_data['Location']

    # load model
    model_dir = '/Users/yuxin.lyx/Documents/practice/ing20200805_python_grad/pycodes/saved_models/lstm'
    # params
    epoch_num = 10

    # # net
    # orig_net = ANNModel(train_x.shape[1], hidden_dim)
    # checkpoint = torch.load("{}/{}.pth".format(model_dir, epoch_num - 1))
    # orig_net.load_state_dict(checkpoint.get('model'))

    # net
    orig_net = LSTMModel(hidden_dim, batch_size, window_dim, train_x.shape[1])
    checkpoint = torch.load("{}/{}.pth".format(model_dir, epoch_num - 1))
    orig_net.load_state_dict(checkpoint.get('model'))

    location_comp = pd.DataFrame(columns=['Location', 'bf_f1score', 'bf_precision', 'bf_recall', 'bf_accuracy', 'bf_auc',
                                          'aft_f1score', 'aft_precision', 'aft_recall', 'aft_accuracy', 'aft_auc'])
    n = 0
    for loc in locations:
        net = copy.deepcopy(orig_net)
        # for param in net.parameters():
        #     param.requires_grad = False
        # num_ftrs = net.fc3.in_features
        # net.fc3 = nn.Linear(num_ftrs, 2)

        # data
        _cdata = cleaned_data[cleaned_data['Location'] == loc]
        _X = _cdata.drop([label_name, 'Location'], axis=1)
        _Y = _cdata[label_name]

        _train_x, _test_x, _train_y, _test_y = train_test_split(_X, _Y, test_size=0.9, shuffle=False, random_state=0)

        _trainset = WeatherTimeSeriesData(_train_x, _train_y, window_dim)
        _testset = WeatherTimeSeriesData(_test_x, _test_y, window_dim)
        _trainloader = torch.utils.data.DataLoader(_trainset, batch_size=batch_size, shuffle=True, num_workers=2,
                                                  drop_last=True)
        _testloader = torch.utils.data.DataLoader(_testset, batch_size=batch_size, shuffle=False, num_workers=2,
                                                 drop_last=True)
        _net, _lstm_evals, _lstm_pred_values = lstm_process(net, _trainloader, _testloader, load_model=False)
        _orig_evals, _ = lstm_pred(orig_net, _testloader)

        # _trainset = WeatherData(_train_x, _train_y)
        # _testset = WeatherData(_test_x, _test_y)
        # _trainloader = torch.utils.data.DataLoader(_trainset, batch_size=batch_size, shuffle=True, num_workers=2, drop_last=True)
        # _testloader = torch.utils.data.DataLoader(_testset, batch_size=batch_size, shuffle=True, num_workers=2, drop_last=True)
        # _ann_model, _ann_evals, _ann_pred_values = ann_process(net, _trainloader, _testloader, load_model=False)
        # _, _orig_evals = ann_pred(orig_net, _testloader)

        model_eval = _lstm_evals
        location_comp.loc[n] = [loc, _orig_evals.get('f1score'), _orig_evals.get('precision'), _orig_evals.get('recall'),
                                    _orig_evals.get('accuracy'), _orig_evals.get('auc'),
                                model_eval.get('f1score'), model_eval.get('precision'), model_eval.get('recall'),
                                model_eval.get('accuracy'), model_eval.get('auc')
                                ]
        n += 1

    location_comp.to_csv('./inter_results/location_comparison_v6.csv')
    print('Finished')



