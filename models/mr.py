from sklearn.linear_model import LogisticRegression
from util.model_metric import modol_metrics

def mr_process(train_x, train_y, test_x, test_y):
    mr = LogisticRegression()
    mr.fit(train_x, train_y)
    pred = mr.predict_proba(test_x)
    pred = pred[:, 1]
    evals = modol_metrics(pred, test_y)
    return mr, evals

