import xgboost as xgb
from util.model_metric import modol_metrics
from xgboost.sklearn import XGBClassifier
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.model_selection import GridSearchCV
from util.file_process import pickle_load, pickle_save
import seaborn as sns


def XGBClassifier_fit(alg, train_x, train_y, useTrainCV=True, cv_folds=5, early_stopping_rounds=50):
    if useTrainCV:
        xgb_param = alg.get_xgb_params()
        xgtrain = xgb.DMatrix(train_x, label=train_y)
        cvresult = xgb.cv(xgb_param, xgtrain, num_boost_round=alg.get_params()['n_estimators'], nfold=cv_folds,
                          metrics='auc', early_stopping_rounds=early_stopping_rounds)
        alg.set_params(n_estimators=cvresult.shape[0])
        print('The optimal n_estimators for learning_rate {} is {}'.format(alg.get_params()['learning_rate'], cvresult.shape[0]))

    # Fit the algorithm on the data
    alg.fit(train_x, train_y, eval_metric='auc')

    # Predict training set:
    dtrain_predprob = alg.predict_proba(train_x)[:, 1]
    train_evals = modol_metrics(dtrain_predprob, train_y)
    print('train_evals', train_evals)

    feat_imp_df = pd.DataFrame(columns=['feature', 'importance'])
    feat_imp_df['feature'] = train_x.columns.values
    feat_imp_df['importance'] = alg.feature_importances_
    feat_imp_df.sort_values("importance", inplace=True, ascending=False)

    return alg, feat_imp_df

def xgb_process(train_x, train_y, test_x, test_y, load_stage1=True, load_stage2=True, load_final=True, plot_feat_imp=False):
    # parameters tuning
    # tune n_estimator
    if load_stage1:
        model1 = pickle_load('./saved_models/xgb/model_202007261146.pkl')
    else:
        test_params1 = XGBClassifier(
            learning_rate=0.1,
            n_estimators=1000,
            max_depth=5,
            min_child_weight=1,
            gamma=0,
            subsample=0.8,
            colsample_bytree=0.8,
            objective='binary:logistic',
            nthread=4,
            scale_pos_weight=1,
            seed=0)
        model1, _ = XGBClassifier_fit(test_params1, train_x, train_y)
        pickle_save(model1, './saved_models/xgb/model_202007261146.pkl')

    # tune max_depth, min_child_weight
    if load_stage2:
        model1 = pickle_load('./saved_models/xgb/model_202007261424.pkl')
    else:
        param_test1 = {
            'max_depth': range(3, 10, 2),
            'min_child_weight': range(1, 6, 2)
        }
        gsearch1 = GridSearchCV(estimator=model1, param_grid=param_test1, scoring='roc_auc', cv=5)
        gsearch1.fit(train_x, train_y)
        print('best_params: {}, best_score: {}'.format(gsearch1.best_params_, gsearch1.best_score_))
        model1.set_params(max_depth=9, min_child_weight=1)
        pickle_save(model1, './saved_models/xgb/model_202007261424.pkl')


    # reduce learning rate
    if load_final:
        model1 = pickle_load('./saved_models/xgb/model_final.pkl')
        model1.set_params(scale_pos_weight=(len(train_y) - sum(train_y)) / sum(train_y))

        feat_imp_df = pd.DataFrame(columns=['feature', 'importance'])
        feat_imp_df['feature'] = train_x.columns.values
        feat_imp_df['importance'] = model1.feature_importances_
        feat_imp_df.sort_values("importance", inplace=True, ascending=False)
    else:
        model1.set_params(learning_rate=0.01)
        model1, feat_imp_df = XGBClassifier_fit(model1, train_x, train_y)
        pickle_save(model1, './saved_models/xgb/model_final.pkl')
    feat_imp_df.to_csv('./inter_results/featureImportance.csv')

    if plot_feat_imp:
        feat_imp_df_tmp = feat_imp_df.iloc[0:20, :]
        f, ax = plt.subplots(figsize=(12, 7))
        ax = sns.barplot(x="feature", y="importance", data=feat_imp_df_tmp)
        plt.xticks(rotation=270)
        plt.show()

    test_pred = model1.predict_proba(test_x)
    test_pred = test_pred[:, 1]
    evals = modol_metrics(test_pred, test_y)
    return model1, evals

