import torch
import torch.autograd as autograd
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from util.model_metric import modol_metrics
import numpy as np

class TDNN(nn.Module):
    def __init__(self, input_dim, output_dim, context_size=1, stride=1, dilation=1, batch_norm=False):
        super(TDNN, self).__init__()
        self.input_dim = input_dim
        self.output_dim = output_dim
        self.context_size = context_size
        self.stride = stride
        self.dilation = dilation
        self.batch_norm = batch_norm
        self.kernel = torch.nn.Linear(input_dim * context_size, output_dim)
        self.unlinearity = torch.nn.ReLU()
        if batch_norm:
            self.bn = torch.nn.BatchNorm1d(output_dim)

    def forward(self, x):
        x = x.unsqueeze(1)
        x = F.unfold(x, (self.context_size, self.input_dim), dilation=(self.dilation,1), stride=(self.stride, self.input_dim))
        x = x.transpose(1,2)
        x = self.kernel(x)
        x = self.unlinearity(x)
        if self.batch_norm:
            x = x.transpose(1,2)
            x = self.bn(x)
            x = x.transpose(1,2)
        return x

class TDNN_LSTM_SGPooling_FC(torch.nn.Module):
    def __init__(self, input_dim=14, tdnn_dim=32, tdnn_layer_num=2, lstm_dim=32, lstm_layer_num=1, fc_dim=16, class_num=2):
        super(TDNN_LSTM_SGPooling_FC, self).__init__()
        self.lstm_dim = lstm_dim
        self.lstm_layer_num = lstm_layer_num
        tdnns = [TDNN(input_dim, tdnn_dim)]
        for i in range(1,tdnn_layer_num):
            tdnns.append(TDNN(tdnn_dim, tdnn_dim))
        self.tdnns = torch.nn.Sequential(*tdnns)
        self.lstm = torch.nn.LSTM(tdnn_dim, lstm_dim, lstm_layer_num, batch_first = True)
        self.fc = torch.nn.Sequential(
                torch.nn.Linear(tdnn_dim * 3, tdnn_dim * 3),
                torch.nn.BatchNorm1d(tdnn_dim * 3),
                torch.nn.ReLU(inplace=True),
                torch.nn.Dropout(0.5),
                torch.nn.Linear(tdnn_dim * 3, fc_dim),
                torch.nn.BatchNorm1d(fc_dim),
                torch.nn.ReLU(inplace=True),
                torch.nn.Dropout(0.5),
                torch.nn.Linear(fc_dim, fc_dim),
                torch.nn.BatchNorm1d(fc_dim),
                torch.nn.ReLU(inplace=True),
                torch.nn.Linear(fc_dim, class_num))

    def forward(self, x):
        x = self.tdnns(x)
        b, h, w = x.shape
        #h0 = torch.randn(self.lstm_layer_num, b, self.lstm_dim)
        #c0 = torch.randn(self.lstm_layer_num, b, self.lstm_dim)
        #x, hn = self.lstm(x, (h0, c0))
        segment_h = h//3
        x1 = x[:,0:segment_h,:].sum(1)
        x2 = x[:,segment_h:segment_h*2,:].sum(1)
        x3 = x[:,segment_h*2:segment_h*3,:].sum(1)
        x = torch.cat((x1,x2,x3),1)
        x = self.fc(x)
        return x

class LSTMModel(nn.Module):

    def __init__(self, hidden_dim, batch_size, window_dim, feature_dim):
        super(LSTMModel, self).__init__()
        self.hidden_dim = hidden_dim
        self.batch_size = batch_size
        self.window_dim = window_dim

        self.lstm = nn.LSTM(feature_dim, hidden_dim, batch_first=True)
        self.hidden = self.init_hidden()
        self.hidden2fc = nn.Linear(window_dim * hidden_dim, hidden_dim)
        self.fc2 = nn.Linear(hidden_dim, hidden_dim)
        self.fc3 = nn.Linear(hidden_dim, 2)

    def init_hidden(self):
        return (autograd.Variable(torch.zeros(1, self.batch_size, self.hidden_dim)),
                autograd.Variable(torch.zeros(1, self.batch_size, self.hidden_dim)))

    def forward(self, x):
        x, self.hidden = self.lstm(x.view(self.batch_size, self.window_dim, -1), self.hidden)
        x = x.reshape(self.batch_size, -1)
        x = F.relu(self.hidden2fc(x))
        x = F.relu(self.fc2(x))
        x = self.fc3(x)
        return x

def lstm_pred(net, testloader):
    net.eval()
    # evaluate model on testset
    pred_value = []
    true_value = []
    softmax = nn.Softmax()
    with torch.no_grad():
        for data in testloader:
            inputs, labels = data
            outputs = net(inputs.float())
            predict = softmax(outputs)
            # _, predict = torch.max(outputs.data, 1)
            pred_value += list(predict.numpy()[:, 1])
            true_value += list(labels.numpy().reshape(1, -1)[0])
            #
            # outputs = net(inputs.float())
            # _, predict = torch.max(outputs.data, 1)
            # pred_value += list(predict.numpy().reshape(1, -1)[0])
            # true_value += list(labels.numpy().reshape(1, -1)[0])

    evals = modol_metrics(pred_value, true_value)
    return evals, pred_value

def lstm_process(net, trainloader, testloader, load_model=True, if_save=False):
    torch.multiprocessing.freeze_support()

    # params
    learning_rate = 1e-5
    adam_weight_decay = 2e-5
    epoch_num = 10
    model_dir = '/Users/yuxin.lyx/Documents/practice/ing20200805_python_grad/pycodes/saved_models/lstm'

    # net
    # net = TDNN_LSTM_SGPooling_FC()
    net.train()
    # define optimizer
    weight = torch.from_numpy(np.array([0.4, 0.6])).float()
    criterion = torch.nn.CrossEntropyLoss(weight=weight)
    optimizer = optim.AdamW(net.parameters(), lr=learning_rate, weight_decay=adam_weight_decay)
    scheduler = torch.optim.lr_scheduler.OneCycleLR(optimizer, max_lr=8e-6, steps_per_epoch=len(trainloader),
                                                    epochs=epoch_num)
    # train
    if not load_model:
        print('Training Start')
        for epoch in range(epoch_num):
            running_loss = 0
            for i, data in enumerate(trainloader):
                inputs, labels = data
                optimizer.zero_grad()
                net.hidden = net.init_hidden()
                # forward
                outputs = net(inputs.float())
                # backward
                loss = criterion(outputs, labels.long())
                # optimize
                loss.backward()
                optimizer.step()
                scheduler.step()
                # statistics
                running_loss += loss.item()
                if i % 500 == 499:
                    test_evals = lstm_pred(net, testloader)
                    print(
                        '[%d, %5d] train loss: %.10f, test_evals: %s' % (epoch + 1, i + 1, running_loss / 500, test_evals))
                    running_loss = 0.0

            state = {'model': net.state_dict(), 'optimizer': optimizer.state_dict(), 'scheduler': scheduler.state_dict(),
                     'epoch': epoch}
            if if_save:
                torch.save(state, "{}/equalweight{}.pth".format(model_dir, epoch))
    else:
        epoch = epoch_num - 1
        checkpoint = torch.load("{}/{}.pth".format(model_dir, epoch))
        net.load_state_dict(checkpoint.get('model'))

    print('Training Finished')
    test_evals, lstm_pred_values = lstm_pred(net, testloader)
    return net, test_evals, lstm_pred_values
