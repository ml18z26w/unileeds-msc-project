import pandas as pd

Locations = ['Beijing', 'Shanghai', 'Shenzhen']
start_date = '2018-01-01'
end_date = '2018-03-01'
date_list = [x.strftime('%Y-%m-%d') for x in list(pd.date_range(start=start_date, end=end_date))]

index = pd.MultiIndex.from_product([date_list, Locations], names=['Day', 'Location'])
init = pd.DataFrame(index=index)
