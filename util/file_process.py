import pickle

def pickle_save(var, file_add):
    file = open(file_add, 'wb')
    pickle.dump(var, file)
    file.close()
    print('File saved in {}'.format(file_add))
    return True

def pickle_load(file_add):
    file = open(file_add, 'rb')
    var = pickle.load(file)
    file.close()
    return var