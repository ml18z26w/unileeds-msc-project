from sklearn.metrics import f1_score, accuracy_score, precision_score, recall_score, roc_curve, auc, confusion_matrix

def modol_metrics(pred_value, true_value, threshold=0.5):
    fpr = None
    tpr = None
    roc_auc = None
    # 若预测值为score，计算roc和auc，并将pred转为0/1
    unique_values = list(set(list(pred_value)))
    if (len(unique_values) != 2 ) | (0 not in unique_values) | (1 not in unique_values):
        fpr, tpr, thresholds = roc_curve(true_value, pred_value)
        roc_auc = auc(fpr, tpr)
        pred_value = [1 if i > threshold else 0 for i in pred_value]

    f1score = f1_score(true_value, pred_value)
    precision = precision_score(true_value, pred_value)
    recall = recall_score(true_value, pred_value)
    accuracy = accuracy_score(true_value, pred_value)
    cfu_matrix = confusion_matrix(true_value, pred_value)
    evals = {
        'f1score': f1score,
        'precision': precision,
        'recall': recall,
        'accuracy': accuracy,
        'fpr': fpr,
        'tpr': tpr,
        'auc': roc_auc,
        'cfu_matrix': cfu_matrix
    }
    return evals