import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.impute import KNNImputer
from sklearn.neighbors import LocalOutlierFactor
import seaborn as sns

# params
data_file_name = './data/weatherAUS.csv'

# read csv
aus_data = pd.read_csv(data_file_name)

# descriptive analysis
desc = aus_data.describe()
desc.to_csv('./inter_results/descriptive_analysis.csv')

# distribution of RainTomorrow
f, ax = plt.subplots(figsize=(10, 6))
ax = sns.countplot(x="RainTomorrow", data=aus_data)
plt.show()

# Distribution of Locations
f, ax = plt.subplots(figsize=(10, 6))
ax = sns.countplot(x="Location", data=aus_data)
plt.xticks(rotation=270)
plt.show()
print(len(aus_data['Location'].unique()))

# one-hot encoding

# convert Date into Year, Month, Day
aus_data['Date'] = pd.to_datetime(aus_data['Date'])
aus_data['Year'] = aus_data['Date'].dt.year
aus_data['Month'] = aus_data['Date'].dt.month
aus_data['Day'] = aus_data['Date'].dt.day

# one-hot encoding
WindGustDir_dms = pd.get_dummies(aus_data['WindGustDir'], dummy_na=True, prefix='WindGustDir')
aus_data = pd.concat([aus_data, WindGustDir_dms], axis=1)

WindDir9am_dms = pd.get_dummies(aus_data['WindDir9am'], dummy_na=True, prefix='WindDir9am')
aus_data = pd.concat([aus_data, WindDir9am_dms], axis=1)

WindDir3pm_dms = pd.get_dummies(aus_data['WindDir3pm'], dummy_na=True, prefix='WindDir3pm')
aus_data = pd.concat([aus_data, WindDir3pm_dms], axis=1)

RainToday_dms = pd.get_dummies(aus_data['RainToday'], dummy_na=True, prefix='RainToday')
aus_data = pd.concat([aus_data, RainToday_dms], axis=1)

aus_data['RainTomorrow'] = aus_data['RainTomorrow'].map(lambda x: 1 if x == 'Yes' else 0)

aus_data = aus_data.drop(['Date', 'Location', 'WindGustDir', 'WindDir9am', 'WindDir3pm', 'RainToday'], axis=1)
# imputation
null_value = aus_data.isnull().sum()
null_value.to_csv('./inter_results/null_value_cnt.csv')
imputer = KNNImputer()
_data = imputer.fit_transform(aus_data)  # it is now transformed into ndarray type, you have to transform into dataframe type
cleaned_data = pd.DataFrame(_data, columns=aus_data.columns.values)
cleaned_null_value = cleaned_data.isnull().sum()
cleaned_null_value.to_csv('./inter_results/cleaned_null_value_cnt.csv')
cleaned_data.to_csv('./inter_results/cleaned_data.csv', index=False)

# boxplot
plt.figure(figsize=(10, 6))
plt.subplot(2, 2, 1)
sns.boxplot( y=aus_data["Rainfall"], palette="Blues")

plt.subplot(2, 2, 2)
sns.boxplot( y=aus_data["Evaporation"], palette="Blues")

plt.subplot(2, 2, 3)
sns.boxplot( y=aus_data["WindGustSpeed"], palette="Blues")

plt.subplot(2, 2, 4)
sns.boxplot( y=aus_data["WindSpeed9am"], palette="Blues")

# identification of outliers
outlier_model = LocalOutlierFactor(n_neighbors=25, contamination=0.005)
outlier_labels = outlier_model.fit_predict(cleaned_data.values)
outlier_labels_df = pd.DataFrame()
outlier_labels_df['label'] = outlier_labels
outlier_labels_df.to_csv('./inter_results/outlier_labels.csv')

oulier_cleaned_data = cleaned_data.values[outlier_labels>0]
oulier_cleaned_data = pd.DataFrame(oulier_cleaned_data)
oulier_cleaned_data.columns = cleaned_data.columns.values

# boxplot after outlier eliminated
plt.figure(figsize=(10, 6))
plt.subplot(2, 2, 1)
sns.boxplot( y=oulier_cleaned_data["Rainfall"], palette="Blues")
plt.subplot(2, 2, 2)
sns.boxplot( y=oulier_cleaned_data["Evaporation"], palette="Blues")
plt.subplot(2, 2, 3)
sns.boxplot( y=oulier_cleaned_data["WindGustSpeed"], palette="Blues")
plt.subplot(2, 2, 4)
sns.boxplot( y=oulier_cleaned_data["WindSpeed9am"], palette="Blues")
oulier_cleaned_data.to_csv('./inter_results/oulier_cleaned_data.csv', index=False)

print('Preprocess end')

oulier_cleaned_data = pd.read_csv('./inter_results/oulier_cleaned_data.csv')
cor_df = oulier_cleaned_data.iloc[:, 0:21]
cor_df = cor_df.drop(['RainTomorrow'], axis=1)
correlation = cor_df.corr()

plt.figure(figsize=(12, 7))
ax = sns.heatmap(correlation, square=True, annot=True, fmt='.1f', linecolor='white')
ax.set_xticklabels(ax.get_xticklabels(), rotation=90)
ax.set_yticklabels(ax.get_yticklabels(), rotation=30)
plt.show()